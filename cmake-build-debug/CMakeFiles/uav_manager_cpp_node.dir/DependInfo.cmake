# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/brent/ws_soa/src/uav_manager_cpp/src/uav_manager_cpp.cpp" "/home/brent/ws_soa/src/uav_manager_cpp/cmake-build-debug/CMakeFiles/uav_manager_cpp_node.dir/src/uav_manager_cpp.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"uav_manager_cpp\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/usr/include/eigen3"
  "/home/brent/ws_soa/devel/.private/multi_mav_manager/include"
  "/home/brent/ws_soa/devel/.private/quadrotor_msgs/include"
  "/home/brent/ws_soa/devel/.private/mav_manager/include"
  "/home/brent/ws_soa/devel/.private/trackers_manager/include"
  "/home/brent/ws_soa/src/quadrotor_control/quadrotor_msgs/include"
  "/home/brent/ws_soa/src/quadrotor_control/trackers/trackers_manager/include"
  "/home/brent/ws_soa/src/quadrotor_control/trackers/std_trackers/include"
  "/home/brent/ws_soa/src/quadrotor_control/mav_manager/include"
  "/home/brent/ws_soa/src/multi_mav_manager/include"
  "/home/brent/ws_soa/src/multi_mav_manager/include/capt/hungarian"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
