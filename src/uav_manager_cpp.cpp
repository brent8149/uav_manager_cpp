/*
 * Brent Schlotfeldt, August 2017
 */


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <Eigen/Dense>
#include <fstream>
#include <map>
#include <string>     // std::string, std::to_string, std::stod
#include <fstream>


#include <multi_mav_manager/RawPosFormation.h>
#include <multi_mav_manager/RawPosFormationRequest.h>
#include <quadrotor_msgs/FlatOutputs.h>

#include <mav_manager/Vec4.h>

using namespace Eigen;


class UAV_Manager
{
public:
    UAV_Manager();
    void callback(const geometry_msgs::PoseStamped::ConstPtr& msg, int i);
    void run();

    MatrixXd load_csv (const std::string & path);
   

private:
    ros::NodeHandle nh_;

    std::vector<std::string> uav_list;
    int n_robots;

    std::map<std::string, Vector3d> uav_pose;
    std::map<int, std::string> index_uav;
    std::vector<ros::Subscriber> sub_list;
    int safety = 1;
};


UAV_Manager::UAV_Manager() {

        nh_ = ros::NodeHandle("~");
        nh_.getParam("safety", safety);
        nh_.getParam("uav_list", uav_list );
        std::cout <<"UAVS included\n";
        for (int i = 0; i < uav_list.size(); i++) {
            std::cout <<  uav_list[i] + ", ";
        }
        n_robots = (int) uav_list.size();
        /*
         * Initialize maps
         *
         */
        for (int i = 0; i < n_robots; i++) {
            index_uav[i] = uav_list[i];
            uav_pose[index_uav[i]] = 1000 * VectorXd::Ones(3,1);
        }

        /*
         * Initialize vicon subscribers
         */
        for (int i = 0; i < n_robots; i++) {
            std::string uav_name = index_uav[i];
            sub_list.push_back(nh_.subscribe<geometry_msgs::PoseStamped>("/vicon/Quadrotor"+uav_name+"/pose",
                                                                         1, boost::bind(&UAV_Manager::callback, this, _1, i)));
        }

    }

    MatrixXd UAV_Manager::load_csv (const std::string & path) {

        std::cout <<"Reading from CSV at : " << path << std::endl;
        std::ifstream indata;
        indata.open(path.c_str());
        std::string line;
        std::vector<double> values;
        uint rows = 0;
        while (std::getline(indata, line)) {
            std::stringstream lineStream(line);
            std::string cell;
            //std::getline
            while (std::getline(lineStream, cell, ',')) {

                std::string::size_type sz;
                values.push_back(std::atof(cell.c_str()));

            }
            ++rows;
        }

        uint cols = values.size()/rows;
        MatrixXd trajectory (rows, cols);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                trajectory(i,j) = values[i * cols + j];
            }
        }

        return trajectory;
    }

    void UAV_Manager::callback( const geometry_msgs::PoseStampedConstPtr& msg, int i) {
        Vector3d pose;
        double x = msg->pose.position.x ;
        double y = msg->pose.position.y;
        double z = msg->pose.position.z;
        pose << x, y, z;
        uav_pose[index_uav[i]] = pose;

    }

    void UAV_Manager::run() {

        ros::Rate loop_rate1(6);

        // Spin once to get vicon poses
        for (int i = 0; i < 20 ; i ++) {
            ros::spinOnce();
            loop_rate1.sleep();
        }

        ros::Rate loop_rate(3.5);
        /*
         * READ CSV files
         */
        std::string trajectory_path;
        nh_.getParam("trajectory_path", trajectory_path);
        std::vector<MatrixXd> trajectories;
        for (int i = 0; i < n_robots; i++) {
            trajectories.push_back(UAV_Manager::load_csv(trajectory_path +
            "traj_robot_" + std::to_string(i+1) + ".csv"));
         //   std::cout << trajectories[i] << std::endl;
        }
        std::cout <<"Read in trajectories from CSV\n";
        if (safety) {
            // Do safety check
            for (int i = 0; i < n_robots; i++) {
                // Compute distances to ensure safe takeoff
                std::string uav_name = index_uav[i];
                Vector3d curr_pose = uav_pose[uav_name];
                Vector4d curr_goal;
                curr_goal = trajectories[i].row(0);
                std::cout << " Curr_goal: " << curr_goal << std::endl;
                double dist = std::sqrt(std::pow(curr_pose(0,0) - curr_goal(0,0),2) +
                                                std::pow(curr_pose(1,0) - curr_goal(1,0), 2));
                std::cout << "Distance of uav: " << uav_name << " " << dist << std::endl;
                if (dist > 1.0) {
                    std::cout << "Distance of uav: " << uav_name << " is not safe: " << dist << ". Terminating\n";
                    exit(1);
                }
            }
        }
        //   ros::Rate loop_rate(1);
        geometry_msgs::PoseStamped msg;

        std::vector<quadrotor_msgs::FlatOutputs> flat_outputs;
        std::vector<ros::ServiceClient> service_clients;

        std::vector<mav_manager::Vec4> vecs;

        for (int i = 0; i < n_robots; i++) {
            std::string uav_name = index_uav[i];
            service_clients.push_back(nh_.serviceClient<mav_manager::Vec4>("/Quadrotor"+uav_name+"/mav_services/goTo"));

        }
        int count = 0;


        while (ros::ok() && count < trajectories[0].rows())
        {
            ros::Time lasttime=ros::Time::now();

            for (int i = 0; i < n_robots; i++) {


                mav_manager::Vec4 temp_vec;
                std::string uav_name = index_uav[i];
                Vector4d waypoint = trajectories[i].row(count);
                std::cout << waypoint << std::endl;
                ROS_INFO("Waypoint_%i: %f, %f \n",count,  waypoint(0,0), waypoint(1, 0));

                boost::array<float ,4> temp_goal = {{waypoint(0,0), waypoint(1,0), 1, 0}};
                temp_vec.request.goal = temp_goal;
                ROS_INFO("Sending WP_%i to %s : %f, %f, %f", count, uav_name.c_str(), waypoint(0,0), waypoint(1,0), 1.0);
                // Call service
                if (service_clients[i].call(temp_vec))
                {
                    ROS_INFO("Messsage %s", temp_vec.response.message.c_str());
                }
                else
                {
                    ROS_ERROR("Failed to call service Messsage %s", temp_vec.response.message.c_str());
                }
            }
            // Send with ROS services
            ros::spinOnce();
            loop_rate.sleep();
            ++count;

            ros::Time currtime=ros::Time::now();
            ros::Duration diff=currtime-lasttime;
            ROS_INFO("tim %g", diff.toSec());
        }
    }

int main(int argc, char **argv)
{
    ros::init(argc, argv, "uav_manager_cpp");
    UAV_Manager mgr = UAV_Manager();


    mgr.run();

    return 0;
}

